package org.clases.proy.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.clases.proy.model.Fichero;
import org.clases.proy.model.UploadFileResponse;
import org.clases.proy.repostirorio.FicheroRepositorio;
import org.clases.proy.service.IUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
public class Controller {
	
	@Autowired
	@Qualifier("UploadSrvLocal")
	IUploadService uploadServiceLocal;
	

	@Autowired
	@Qualifier("UploadSrvBBDD")
	IUploadService uploadServiceBBDD;

	@Autowired
	FicheroRepositorio ficheroRepositorio;


	@PostMapping("/file")
	@ResponseStatus(code = HttpStatus.CREATED)
	public UploadFileResponse uploadFIle(@RequestParam MultipartFile file) {
		String fileNameLocal = uploadServiceLocal.storeFile(file);

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/file/")
                .path(fileNameLocal)
                .toUriString();
        
        //
//        String storeFileBBDD = 
		uploadServiceBBDD.storeFile(file);
        
        
        
        return new UploadFileResponse(fileNameLocal, fileDownloadUri,
                file.getContentType(), file.getSize());

	}

	@GetMapping("/file/{nombreFich}")
	@ResponseStatus(code = HttpStatus.OK)
	public ResponseEntity<Resource> uploadFIle2( @PathVariable String nombreFich, HttpServletRequest req) {
		System.out.println("intentando traer "+nombreFich);
		
		Resource resource = uploadServiceLocal.loadFileAsResource(nombreFich);
		String mimeType=null;
		try {
			mimeType = req.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		} catch (IOException e) {
			e.printStackTrace();
		}
        if(mimeType == null) {
        	mimeType = "application/octet-stream";
        }
		return ResponseEntity.ok()
				.contentType(MediaType.parseMediaType(mimeType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
				.body(resource);
	}

	@GetMapping("/filebd/{nombreFich}")
	@CrossOrigin()
	@ResponseStatus(code = HttpStatus.OK)
	public ResponseEntity<Resource> uploadFIleBD( @PathVariable String nombreFich, HttpServletRequest req) {
		
		Fichero findByFilename = ficheroRepositorio.findByFilename(nombreFich);
		
		Resource resource = new ByteArrayResource(findByFilename.getData());
		String mimeType=findByFilename.getType();
		
        if(mimeType == null) {
        	mimeType = "application/octet-stream";
        }
        
		return ResponseEntity.ok()
				.contentType(MediaType.parseMediaType(mimeType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
				.body(resource);
	}
}
