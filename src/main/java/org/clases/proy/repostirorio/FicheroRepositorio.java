package org.clases.proy.repostirorio;

import org.clases.proy.model.Fichero;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FicheroRepositorio extends JpaRepository<Fichero, String>{
	
	Fichero findByFilename(String filename);

}
