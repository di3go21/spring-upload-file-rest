package org.clases.proy.cors;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class CorsConfig implements WebMvcConfigurer {
	
	@Value("${dominios-cliente}")
	private List<String> dominios;
	

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/file/**")
		.allowedOrigins("*")
		.allowedMethods("GET","POST");
		dominios.forEach(dominio->registry.addMapping("/file/**")
				.allowedOrigins(dominio) //esta url tiene que ser desde donde pides la URL
				.allowedMethods("GET","POST")
		);
	}
}
