package org.clases.proy.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class MiFicheroNoEncontradoException extends RuntimeException{
	
	private static final long serialVersionUID = -825021174834413347L;
	
	public MiFicheroNoEncontradoException(String message) {
		super(message);
	}
	public MiFicheroNoEncontradoException(String message, Throwable causa) {
		super(message,causa);
	}

}
