package org.clases.proy.exception;

public class FilestorageException extends RuntimeException {

	private static final long serialVersionUID = 5190226499591437826L;
	
	public FilestorageException(String message) {
		super(message);
	}
	public FilestorageException(String message,Throwable e) {
		super(message,e);
	}

}
