package org.clases.proy.service;

import java.io.IOException;

import org.clases.proy.exception.FilestorageException;
import org.clases.proy.model.Fichero;
import org.clases.proy.repostirorio.FicheroRepositorio;
import org.hibernate.cfg.NotYetImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


@Service
@Qualifier("UploadSrvBBDD")
public class UploadServiceBBDD implements IUploadService{
	
	@Autowired
	FicheroRepositorio ficheroRepositorio;

	@Override
	public String storeFile(MultipartFile file) {
		Fichero fichero = new Fichero();
		
		fichero.setFilename(file.getOriginalFilename());
		fichero.setType(file.getContentType());
		try {
			fichero.setData(file.getBytes());
		} catch (IOException e) {
			throw new FilestorageException("Error al intentar leer el fichero subido");
		}
		
		ficheroRepositorio.save(fichero);
		
		return fichero.getFilename();
	}

	@Override
	public Resource loadFileAsResource(String fileName) {
		
		throw new NotYetImplementedException("no implementado");
//		Fichero file = ficheroRepositorio.findByFilename(fileName);
//		
////		return new ByteArrayResource(file.get)
//		
//		
//		// TODO Auto-generated method stub
//		return null;
	}
	

}
