package org.clases.proy.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface IUploadService {

	String storeFile(MultipartFile file);

	Resource loadFileAsResource(String fileName);

}