package org.clases.proy.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.clases.proy.exception.FilestorageException;
import org.clases.proy.exception.MiFicheroNoEncontradoException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@Qualifier("UploadSrvLocal")
public class UploadServiceLocal implements IUploadService {
	
	private final Path carpetaUpload;

	
	public UploadServiceLocal(@Value("${file.upload-dir}") String path) {
		this.carpetaUpload=Paths.get(path).toAbsolutePath().normalize();
	}
	
	@Override
	public String storeFile(MultipartFile file) {
		
		String fileName = file.getOriginalFilename();
		
		if(fileName.contains("..")) {
			throw new FilestorageException("No se puede subir nombre de archivos con ..");
		}
		
		Path pathDestino= this.carpetaUpload.resolve(fileName);
		try {
			Files.copy(file.getInputStream(),pathDestino,StandardCopyOption.REPLACE_EXISTING);
			return fileName;
		} catch (IOException e) {
			throw new FilestorageException("No se pudo Subir el fichero "+ fileName+" intentelo de nuevo", e);
		}
		
	}
	
	@Override
	public Resource loadFileAsResource(String fileName) {
		Path rutaFichero = this.carpetaUpload.resolve(fileName).normalize();
		
		try {
			Resource res = new UrlResource(rutaFichero.toUri());
			if(res.exists())
				return res;
			throw new MiFicheroNoEncontradoException("fichero no encontrado "+fileName);
		} catch (MalformedURLException e) {
			throw new MiFicheroNoEncontradoException("fichero no encontrado "+fileName, e);
		}
	}
	
}
