package org.clases.proy.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

import lombok.Data;

@Data
@Entity
public class Fichero {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String filename;
	
	private String type;
	
	@Lob
	private byte[] data;
}
